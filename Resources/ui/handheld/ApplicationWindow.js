function ApplicationWindow(win) {
    var AnimationComponents = require('ui/common/AnimationComponents');
    var AnimationTable = require('ui/common/AnimationTable');
    
    var self = Ti.UI.createWindow({
        backgroundColor:'#ffffff'
    });
    
    // AnimationComponents.insert(self);
    AnimationTable.insert(self);

    return self;
}

module.exports = ApplicationWindow;
