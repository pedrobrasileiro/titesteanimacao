exports.insert = function(win) {
    win.circle = Ti.UI.createView({
        backgroundColor : '#CCCCCC',
        height : 30,
        width : 30,
        top : 50,
        borderRadius : 15
    });

    win.add(win.circle);

    win.circleDrag = null;
    
    win.totalBarDrag = Ti.UI.createView({
        backgroundColor:'#7B961F',
        height : 1, 
        width : '100%', 
        bottom : 0
    });
    
    win.add(win.totalBarDrag);

    win.circle.addEventListener('longpress', function(e){
        win.circleDrag = Ti.UI.createView({
            backgroundColor : '#000000',
            height : 30,
            width : 30,
            top : 50,
            borderRadius : 15
        });

        win.circleDrag.addEventListener('touchmove', function(e) {
            var newX = e.x + win.circleDrag.animatedCenter.x - win.circleDrag.width/2;
            var newY = e.y + win.circleDrag.animatedCenter.y - win.circleDrag.height/2;
            win.circleDrag.animate({
                left : newX,
                top : newY,
                duration : 1
            });
        });

        win.circleDrag.addEventListener('touchend', function(e) {
            win.add(win.circle);
            win.remove(win.circleDrag);
            win.circleDrag = null;

            // Voltando a cor original
            win.backgroundColor = '#FFFFFF';
            
            // Ocultando a totalBar
            win.totalBarDrag.animate({
                height : 1
            });
        });

        // Passando o evento do original para a cópia
        win.circle.fireEvent('touchcancel');
        win.circleDrag.fireEvent('touchstart');

        // Ocultando a original e deixando só a cópia visível
        win.add(win.circleDrag);
        win.remove(win.circle);

        // Mudando a opacidade do fundo
        win.backgroundColor = '#CCCCCC';
        
        // Animando abertura da barra de total
        win.totalBarDrag.animate({
            height : 50
        });
    });

    return win;
}