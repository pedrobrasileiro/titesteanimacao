exports.insert = function (win) {
    var totalizeBar = getTotalizeBar();
    win.add(totalizeBar);
    
    nomes = [ 
        {title : 'Pedro', valor : 12}, 
        {title : 'Ivan', valor : 14}, 
        {title : 'Israel', valor : 16.3}, 
        {title : 'Roberta', valor : 5.65}
     ];

    rows = [];
    for (var i = 0; i < nomes.length; i++) {
        rows[i] = createRow({
            win : win, 
            title : nomes[i].title, 
            price : nomes[i].valor, 
            bar : totalizeBar
        });
    }

    var table = Ti.UI.createTableView({
        data : rows, 
        top : 0, 
        height : '50%'
    });

    win.add(table);
};

function AinB (a, b) {
    console.debug("A - w:"+ a.rect.width +" h:"+ a.rect.height +" x:"+ a.rect.x +" y:"+ a.rect.y);
    console.debug("B - w:"+ b.rect.width +" h:"+ b.rect.height +" x:"+ b.rect.x +" y:"+ b.rect.y);

    var inHeight    = (a.rect.y + a.rect.height >= b.rect.y) && (a.rect.y <= b.rect.y + b.rect.height);
    var inWidth     = (a.rect.x + a.rect.width >= b.rect.x) && (a.rect.x <= b.rect.x + b.rect.width);

    return inHeight && inWidth;
}

function createRow(args) {
    var row = Ti.UI.createTableViewRow({
        width : '40dp', 
        height : Ti.UI.SIZE
    });

    var label = Ti.UI.createLabel({
        text : args.title, 
        width : Ti.UI.FILL, 
        height : Ti.UI.SIZE
    });

    label.addEventListener('longpress', function(e) {
        console.debug("Linha: "+ e.source +" - "+ e.type +" - "+ e.x +"/"+ e.y);
        
        args.bar.animatedOpen();
        
        var labelDrag = getLabelDrag({ 
            win : args.win, 
            title : args.title, 
            price : args.price, 
            left : e.x, 
            top : e.y, 
            bar : args.bar
        });
        args.win.add(labelDrag);
    });

    row.add(label); 
    return row;
};

function getLabelDrag(args) {
    var self = Ti.UI.createLabel({
        text : (args.title +"  R$:"+ args.price), 
        width : 200, 
        height : 40, 
        left : args.left, 
        top : args.top, 
        backgroundColor : '#A99B14', 
        borderColor : '#000000', 
        borderRadius : 5
    });

    self.addEventListener('touchmove', function(e) {
        var newX = e.x + self.animatedCenter.x - self.width/2;
        var newY = e.y + self.animatedCenter.y - self.height/2;
        
        args.bar.isIn(self);
        
        self.animate({
            left : newX,
            top : newY,
            duration : 1
        });
    });

    self.addEventListener('touchend', function(e) {
        console.debug("Label removed - Last x/y: "+ self.left +"/"+ self.top);
        
        if (args.bar.isIn(self)) {
            args.bar.sum(args.price);
        }

        args.win.remove(self);
    });

    return self;
};

function getTotalizeBar(args) {
    var total = 0;
    var defaultColor = '#6F7748';
    var alternativeColor = "#81E93B";
    
    var self = Ti.UI.createView({
        bottom : 0, 
        width : Ti.UI.FILL, 
        height : 1, 
        backgroundColor : defaultColor
    });

    self.addEventListener('click', function (e) {
        if (e.source == self) {
            self.animatedClose();
        }
    });

    var label = Ti.UI.createLabel({
        text : String.formatCurrency(total), 
        width : Ti.UI.SIZE, 
        height : Ti.UI.FILL, 
        top : 0, 
        left : 0
    });

    self.add(label);
    
    var cleanView = Ti.UI.createImageView({
        image : 'trash.png', 
        right : 5, 
        bottom : 10
    });
    
    cleanView.addEventListener('click', function (e) {
        self.sum(total * -1);
    });
    
    self.add(cleanView);

    self.animatedOpen = function () {
        self.animate({
            height : 50
        });
    }

    self.animatedClose = function () {
        self.animate({
            height : 1
        });
    }

    self.isIn = function (view) {
        var isIn = AinB(view, self);
        
        if (isIn) {
            self.backgroundColor = alternativeColor;
        } else {
            self.backgroundColor = defaultColor;
        }
        
        return isIn;
    };

    self.sum = function (price) {
        total += price;
        self.backgroundColor = defaultColor;
        label.text = String.formatCurrency(total);
    }
    
    return self;
}